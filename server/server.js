const io = require('socket.io')(3000, {
    cors: {
        origin: ["http://192.168.0.107:5173"] // Change to local exposed IP
    }
});

io.on('connection', socket => {
    console.log(socket.id);
    socket.on('send-message', (message, room) => {
        if (room === "") {
            socket.broadcast.emit('receive-message', message);
        } else {
            socket.to(room).emit('receive-message', message);
        }
    });
    socket.on('join-room', (room, callback) => {
        socket.join(room);
        if (room === "") {
            callback(`Entrou no servidor geral`);
        }
        callback(`Entrou no servidor: ${room}`);
    });
});