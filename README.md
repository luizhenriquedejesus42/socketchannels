<h1 align="center"><b>Chat Forum App</b></h1>

<p align="left">Chat Forum App is a real-time chat application that allows users to communicate with each other using text messages. The app is built using the latest web technologies, including Socket.io, Node.js, and Vite, which enable high-speed, reliable, and scalable communication.</p>
<p align="left">With this app, users can create their own chat rooms and invite others to join. They can send and receive text messages and collaborate with others in real-time. The app provides a smooth and seamless chat experience with no lag or delays.</p>

<br>

<h3 align="left"><b>Some of the key features of the Chat Forum App include:</b></h3>
<ul>
<li>Real-time messaging: The app enables users to chat with each other in real-time, allowing for fast and efficient communication.</li>
<li>Chat rooms: Users can create their own chat rooms and invite others to join, providing a personalized experience.</li>
<li>Secure messaging: Messages are not stored, ensuring that your conversations remain confidential.</li>
<li>Overall, the Chat Forum App provides a modern and user-friendly chat experience that is perfect for businesses, communities, and individuals looking to connect and collaborate with others.</li>
</ul>

<br>
<hr>
<br>

<h3 align="left"><b>How to clone the repository and start the application:</b></h3>
<p align="left">Make sure you have installed Node.js and npm to run the application.</p>

<br>

<p align="left">Cloning the repository (Choose one):</p>
<code>git clone https://gitlab.com/janinemilena/socketchannels.git</code><br>
<code>git clone git@gitlab.com:janinemilena/socketchannels.git</code>

<br>

<p align="left">Installing Node modules:</p>
<code>npm install</code>

<br>

<p align="left">Starting the application:</p>
<p align="left">Client:</p>
<code>cd client</code>
<br>
<code>npm run dev</code>

<br>

<p align="left">Server:</p>
<code>cd server</code>
<br>
<code>npm run dev</code>

<br>

<p align="left">Change the network exposed IP (Network IP exposed when starting client application):</p>
<p align="left">In <code>/client/script.js</code>:</p>
<code>const socket = io("http://LOCAL_NETWORK_IP:3000"); </code>

<br>

<p align="left">In <code>/server/server.js</code>:</p>
<code>origin: ["http://LOCAL_NETWORK_IP:5173"] </code>

<br>

<h3><b>You're done!</b></h3> 

<hr>
<p align="center">Desenvolvimento para Web - Unochapecó - 2023</p>